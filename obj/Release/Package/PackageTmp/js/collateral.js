﻿var url = "http://localhost"

function mask() {
    $('.number').mask("000.000.000", { placeholder: "000.000.000", reverse: true });
    $('.datepicker').mask("00/00/0000", { placeholder: "__/__/____" });
}

function getCTPY(p_Party) {
    var porta = "10007"
    
    if (p_Party == "Itau") {
        porta = "10007"
    }
    else {
        porta = "10010"
    }
    
    var api = url + ":"+porta+"/api/collateral/peers";

    $.getJSON(api, function (response) {

        //var options = $("#comboContraparte");
        $.each(response, function () {
            $("#comboContraparte").append($("<option />").val(this).text(this));
        });
    });
}

function onSelectCTPY(p_Party) {
    var api = url
    if (p_Party == "Itau") {
        api = api + ":10007/api/collateral/listar-agreements";
    }
    else {
        api = api + ":10010/api/collateral/listar-agreements";
    }

    var ctpy = $("#comboContraparte option:selected").text()

    $.getJSON(api + "?ctpy=" + ctpy, function (response) {
        for (i = 0; i < response.length; i++) {
            var state = response[i];

            $("#tblAgreement tbody").append(" <tr id=" + state.agreement.idAgreement + ">" +
                                        "<td style=\"text-align:center\">" + state.agreement.party + "</td>" +
                                        "<td style=\"text-align:center\">" + state.agreement.ctpy + "</td>" +
                                        "<td style=\"text-align:center\">" + state.agreement.minimal + "</td>" +
                                        "<td style=\"text-align:center\">" + state.agreement.maximal + "</td>" +
                                        "<td style=\"text-align:center\">" + state.agreement.currency + "</td>" +
                                        "<td style=\"text-align:center\">" + state.agreement.type + "</td>" +
                                        "<td style=\"text-align:center\">" + state.timeStamp + "</td>" +
                                    "</tr>");
        }
    });
}

function criarAgreement(p_Party) {
    var node = "";

    if (p_Party == "Itau") {
        node = "10007"
    } else {
        node = "10010"
    }

    var agreement = {
        party: p_Party,
        ctpy: $("#comboContraparte option:selected").text(),
        minimal: parseFloat($("#min").val().split(".").join("").replace(",", ".")),
        maximal: parseFloat($("#minCall").val().split(".").join("").replace(",", ".")),
        currency: $("#currency option:selected").text(),
        type: $("#comboType option:selected").text()
    }

    var api = url + ":" + node + "/api/collateral/agreement?"
                            + "party=" + agreement.party
                            + "&ctpy=" + agreement.ctpy
                            + "&minimal=" + agreement.minimal
                            + "&maximal=" + agreement.maximal
                            + "&currency=" + agreement.currency
                            + "&type=" + agreement.type;

    $.ajax({
        type: "GET",
        contentType: "text/plain",
        url: api,
        success: function (data) {
            alert(data)
        },
        error: function (data) {
            alert(data.responseText)
        }
    });
}

function registrarChamada(p_Party) {
    var node = "";

    if (p_Party == "Itau") {
        node = "10007"
    } else {
        node = "10010"
    }

    var chamada = {
        idExternalParty: $("#idExternalParty").val(),
        idExternalCTPY: "",
        party: p_Party,
        ctpy: $("#comboContraparte option:selected").text(),
        exposure: parseFloat($("#exposure").val().split(".").join("").replace(",", ".")),
        marginCall: parseFloat($("#marginCall").val().split(".").join("").replace(",", ".")),
        callDate: $("#database").val(),
        currency: $("#currency option:selected").text()
    }


    var api = url + ":" + node + "/api/collateral/chamada?idExternalParty=" + chamada.idExternalParty
                            + "&idExternalCTPY=" + chamada.idExternalCTPY
                            + "&exposure=" + chamada.exposure
                            + "&marginCall=" + chamada.marginCall
                            + "&currency=" + chamada.currency;

    $.ajax({
        type: "GET",
        contentType: "text/plain",
        url: api,
        success: function (data) {
            alert(data)
        },
        error: function (data) {
            alert(data)
        }
    });
}

function aprovarChamada(p_Comments, p_Party, p_idMargin, p_isParty) {
    var node = "";

    if (p_Party == "Itau") {
        node = "10007"
    } else {
        node = "10010"
    }

    var api = url + ":"+ node + "/api/collateral/aprovar-chamada?contractAddress=" + p_idMargin
                                + "&newComments=" + p_Comments;

    $.ajax({
        type: "GET",
        contentType: "text/plain",
        url: api,
        success: function (data) {
            alert(data)

            $("#tblPosicao tbody").empty()

            if (p_isParty) {
                carregarChamadasRealizadas(p_Party)
            } else {
                carregarChamadasRecebidas(p_Party)
            }
        },
        error: function (data) {
            alert(data.responseText)
        }
    });
}

function aprovarChamadaModal(p_Party, p_idMargin, p_isParty) {
    var html = "";


    html =   " <label for=\"comments\">Comments</label>" +
             " <input id=\"comments\" class=\"form-control\" min=\"0\" style=\"text-align:right\">"

    showBSModal({
        title: "Accept",
        body: html,
        size: "small",
        actions: [{
            label: 'Concluir',
            cssClass: 'btn-success',
            onClick: function (e) {
                aprovarChamada($('#comments').val(), p_Party, p_idMargin, p_isParty);
                $(e.target).parents('.modal').modal('hide');
            }
        }, {
            label: 'Cancelar',
            cssClass: 'btn-danger',
            onClick: function (e) {
                $(e.target).parents('.modal').modal('hide');
            }
        }]
    });
}

function contrapropostaChamadaModal(p_Exposure, p_MarginCall, p_Party, p_idMargin, p_isParty) {
    var html = "";


      html =   "<label for=\"exposureCP\">New Exposure</label>" +
               "<input id=\"exposureCP\" class=\"form-control number\" min=\"0\" style=\"text-align:right\" value=\"" + p_Exposure + "\">" +
                "<label for=\"marginCallCP\">New Margin Call</label>" +
                "<input id=\"marginCallCP\" class=\"form-control number\" min=\"0\" style=\"text-align:right\" value=\"" + p_MarginCall + "\">" +
                " <label for=\"comments\">Comments</label>" +
                " <input id=\"comments\" class=\"form-control\" min=\"0\" style=\"text-align:right\">" +
                "<input type=\"file\" name=\"contraPropostaFile\" id=\"contraPropostaFile\">"

    showBSModal({
        title: "Contraproposta",
        body: html,
        size: "small",
        actions: [{
            label: 'Concluir',
            cssClass: 'btn-success',
            onClick: function (e) {
                contrapropostaChamada(parseFloat($('#exposureCP').val().split(".").join("").replace(",", ".")), parseFloat($('#marginCallCP').val().split(".").join("").replace(",", ".")), $('#comments').val(), p_Party, p_idMargin, p_isParty);
                $(e.target).parents('.modal').modal('hide');
            }
        }, {
            label: 'Cancelar',
            cssClass: 'btn-danger',
            onClick: function (e) {
                $(e.target).parents('.modal').modal('hide');
            }
        }]
    });
}

function contrapropostaChamada(p_Exposure, p_MarginCall, p_Comments ,p_Party, p_idMargin, p_isParty) {
    var node = "";

    if (p_Party == "Itau") {
        node = "10007"
    } else {
        node = "10010"
    }

    var api = url + ":" + node + "/api/collateral/contraproposta-chamada?contractAddress=" + p_idMargin
                                + "&newExposure=" + parseFloat(p_Exposure)
                                + "&newMarginCall=" + parseFloat(p_MarginCall)

    $.ajax({
        type: "GET",
        contentType: "text/plain",
        url: api,
        success: function (data) {
            alert(data)

            $("#tblPosicao tbody").empty()

            if (p_isParty) {
                carregarChamadasRealizadas(p_Party)
            } else {
                carregarChamadasRecebidas(p_Party)
            }
        },
        error: function (data) {
            alert(data.responseText)
        }
    });
}

function settleChamadaModal(p_Party, p_idMargin, p_isParty) {
    var html = "";


    html = " <label for=\"ammountSettled\">Amount Settled</label>" +
             " <input id=\"ammountSettled\" class=\"form-control number\" min=\"0\" style=\"text-align:right\" value=\"" + 0 + "\">" +
             " <label for=\"comments\">Comments</label>" +
             " <input id=\"comments\" class=\"form-control\" min=\"0\" style=\"text-align:right\">"

    showBSModal({
        title: "Settled",
        body: html,
        size: "small",
        actions: [{
            label: 'Concluir',
            cssClass: 'btn-success',
            onClick: function (e) {
                settleChamada(parseFloat($('#ammountSettled').val().split(".").join("").replace(",", ".")), $('#comments').val(), p_Party, p_idMargin, p_isParty);
                $(e.target).parents('.modal').modal('hide');
            }
        }, {
            label: 'Cancelar',
            cssClass: 'btn-danger',
            onClick: function (e) {
                $(e.target).parents('.modal').modal('hide');
            }
        }]
    });
}

function settleChamada(p_AmmountSettled, p_Comments, p_Party, p_idMargin, p_isParty) {
    var node = "";

    if (p_Party == "Itau") {
        node = "10007"
    } else {
        node = "10010"
    }

    var api = url + ":" + node + "/api/collateral/settle-chamada?contractAddress=" + p_idMargin
                            + "&newSettleAmount=" + p_AmmountSettled
                            + "&newComments=" + p_Comments;

    $.ajax({
        type: "GET",
        contentType: "text/plain",
        url: api,
        success: function (data) {
            alert(data)

            $("#tblPosicao tbody").empty()

            if (p_isParty) {
                carregarChamadasRealizadas(p_Party)
            } else {
                carregarChamadasRecebidas(p_Party)
            }
        },
        error: function (data) {
            alert(data.responseText)
        }
    });
}

function cancelarChamada(p_Comments, p_Party, p_idMargin, p_isParty) {
    var node = "";

    if (p_Party == "Itau") {
        node = "10007"
    } else {
        node = "10010"
    }

    var api = url + ":" + node + "/api/collateral/cancelar-chamada?contractAddress=" + p_idMargin
                                + "&newComments=" + p_Comments;

    $.ajax({
        type: "GET",
        contentType: "text/plain",
        url: api,
        success: function (data) {
            alert(data)

            $("#tblPosicao tbody").empty()

            if (p_isParty) {
                carregarChamadasRealizadas(p_Party)
            } else {
                carregarChamadasRecebidas(p_Party)
            }
        },
        error: function (data) {
            alert(data.responseText)
        }
    });
}

function cancelarChamadaModal(p_Party, p_idMargin, p_isParty) {
    var html = "";


    html =   " <label for=\"comments\">Comments</label>" +
             " <input id=\"comments\" class=\"form-control\" min=\"0\" style=\"text-align:right\">"

    showBSModal({
        title: "Cancel",
        body: html,
        size: "small",
        actions: [{
            label: 'Concluir',
            cssClass: 'btn-success',
            onClick: function (e) {
                cancelarChamada($('#comments').val(), p_Party, p_idMargin, p_isParty);
                $(e.target).parents('.modal').modal('hide');
            }
        }, {
            label: 'Cancelar',
            cssClass: 'btn-danger',
            onClick: function (e) {
                $(e.target).parents('.modal').modal('hide');
            }
        }]
    });
}

function habilitarInput(checkBox, input) {
    input.prop("value", "");
    input.prop("disabled", !checkBox.checked);
}

function loadPage(p) {
    $("#content-div").load(p)
}

function expandir(linhasExpandir, checkbox) {
    for (var i = 0, row; row = linhasExpandir[i]; i++) {
        if (checkbox.checked) {
            row.style.display = 'table-row';
        }
        else
            row.style.display = 'none';
    }
}

function carregarChamadasRealizadas(p_Party) {

    var api = url
    if (p_Party == "Itau") {
        api = api + ":10007/api/collateral/listar-chamadas-realizadas";
    }
    else {
        api = api + ":10010/api/collateral/listar-chamadas-realizadas";
    }

    $.getJSON(api, function (response) {
        for (i = 0; i < response.length; i++) {
            var state = response[i];
            
            var action = "";

            if (state.status == "Margin Call") {
                action = "<td style=\"text-align:center\"><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"cancelarChamadaModal('" + p_Party + "','" + state.contractAddress + "', true)\">Cancel</button></td>";
            } else if (state.status == "Disputed") {''
                action = "<td style=\"text-align:center\"><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"aprovarChamadaModal('" + p_Party + "','" + state.contractAddress + "', true)\">Full Agree</button>" +
                    "<button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"cancelarChamadaModal('" + p_Party + "','" + state.contractAddress + "', true)\">Cancel</button>" +
                                                         "<button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"contrapropostaChamadaModal(" + state.exposure + 
                                                         "," + state.marginCall + 
                                                         ",'" + p_Party + 
                    "','" + state.contractAddress +
                                                         "', true)\">Dispute</button></td>";
            } else if (state.status == "Agreed") {''
                action = "<td style=\"text-align:center\"><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"settleChamadaModal('" + p_Party + "','" + state.contractAddress + "', true)\">Settle</button></td>";
            } else if (state.status == "Settled") {
                action = "<td style=\"text-align:center\">Settled</td>";
            } else if (state.status == "Cancelled") {
                action = "<td style=\"text-align:center\">Cancelled</td>";
            }

            var trBk = "";

            if (state.status == "Cancelled") {
                trBk = "class=\"bg-danger\"";
            } else if (state.status == "Agreed") {
                trBk = "class=\"bg-info\"";
            } else if (state.status == "Settled") {
                trBk = "class=\"bg-success\"";
            }
            
            $("#tblPosicao tbody").append(" <tr " + trBk + "id=" + state.contractAddress + ">" +
                "<td style=\"text-align:center\"><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"abrirHistorico('" + p_Party + "','" + state.contractAddress + "', true)\">+</button></td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalParty + "</td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalCTPY + "</td>" +
                                        "<td style=\"text-align:center\">" + state.ctpy + "</td>" +
                                        "<td style=\"text-align:center\">" + state.currency + " " + state.exposure + "</td>" +
                                        "<td style=\"text-align:center\">" + state.currency + " " + state.marginCall + "</td>" +
                                        "<td style=\"text-align:center\">" + state.callDate + "</td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalParty + "</td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalCTPY + "</td>" +
                                        "<td style=\"text-align:center\">" + state.status + "</td>" +
                                        "<td style=\"text-align:center\">" + state.timeStamp + "</td>" +
                                        "<td style=\"text-align:center\">" + state.amountSettle + "</td>" +
                                        action +
                                    "</tr>");
        }
    });
}


function carregarChamadasRecebidas(p_Party) {

    var api = url
    if (p_Party == "Itau") {
        api = api + ":10007/api/collateral/listar-chamadas-recebidas";
    }
    else {
        api = api + ":10010/api/collateral/listar-chamadas-recebidas";
    }

    $.getJSON(api, function (response) {
        for (i = 0; i < response.length; i++) {
            var state = response[i];

            var action = "";

            if (state.status == "Margin Call") {
                action = "<td style=\"text-align:center\"><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"contrapropostaChamadaModal(" + state.exposure + 
                                                         "," + state.marginCall + 
                                                         ",'" + p_Party + 
                    "','" + state.contractAddress +
                                                         "', false)\">Dispute</button>" +
                    "<button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"aprovarChamadaModal('" + p_Party + "','" + state.contractAddress + "', false)\">Agree</button></td>";
            } else if (state.status == "Disputed") {
                action = "<td style=\"text-align:center\">Disputed</td>";
            } else if (state.status == "Agreed") {
                action = "<td style=\"text-align:center\">Agree</td>";
            } else if (state.status == "Settled") {
                action = "<td style=\"text-align:center\">Settled</td>";
            } else if (state.status == "Cancelled") {
                action = "<td style=\"text-align:center\">Cancelled</td>";
            }

            var trBk = "";

            if (state.status == "Cancelled") {
                trBk = "class=\"bg-danger\"";
            } else if (state.status == "Agreed") {
                trBk = "class=\"bg-info\"";
            } else if (state.status == "Settled") {
                trBk = "class=\"bg-success\"";
            }

            $("#tblPosicao tbody").append(" <tr " + trBk + "id=" + state.contractAddress + ">" +
                "<td style=\"text-align:center\"><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"abrirHistorico('" + p_Party + "','" + state.contractAddress + "', false)\">+</button></td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalParty + "</td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalCTPY + "</td>" +
                                        "<td style=\"text-align:center\">" + state.party + "</td>" +
                                        "<td style=\"text-align:center\">" + state.currency + " " + state.exposure + "</td>" +
                                        "<td style=\"text-align:center\">" + state.currency + " " + state.marginCall + "</td>" +
                                        "<td style=\"text-align:center\">" + state.callDate + "</td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalParty + "</td>" +
                                        "<td style=\"text-align:center;display:none\">" + state.idExternalCTPY + "</td>" +
                                        "<td style=\"text-align:center\">" + state.status + "</td>" +
                                        "<td style=\"text-align:center\">" + state.timeStamp + "</td>" +
                                        "<td style=\"text-align:center\">" + state.amountSettle + "</td>" +
                                        action +
                                    "</tr>");
        }
    });
}

function abrirHistorico(p_Party, contractAddress, p_isParty) {

    var node = "";

    if (p_Party == "Itau") {
        node = ":10007/";
    }
    else {
        node = ":10010/";
    }

    var state0;
    var html = "";

    $.getJSON(url + node + "api/collateral/" + (p_isParty ? "listar-chamadas-realizadas" : "listar-chamadas-recebidas") + "?contractAddress=" + contractAddress, function (response) {
        for (i = 0; i < response.length; i++) {
            state0 = response[i];
        }

        html = "<div  style=\"border: solid; border-color:green\">" +
                 "<table class=\"table table-inverse table-bordered\">" +
                    "<tr>" +
                        "<td style=\"font-weight:bold;\">Party:</td><td style=\"text-align:center;\">" + state0.party + "</td><td style=\"font-weight:bold;\">External ID Party:</td><td style=\"text-align:center\">" + (state0.idExternalParty == null ? "" : state0.idExternalParty) + "</td><td style=\"font-weight:bold;\">Attachment</td><td><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"getDocument('" + p_Party + "','" + "" + "','" + "" + "')\">" + "" + "</button></td>" +
                    "</tr><tr>" +
                        "<td style=\"font-weight:bold;\">Counterparty:</td><td style=\"text-align:center\">" + state0.ctpy + "</td><td style=\"font-weight:bold;\">External ID Counterparty:</td><td style=\"text-align:center\">" + (state0.idExternalCTPY == null ? "" : state0.idExternalCTPY) + "</td><td style=\"font-weight:bold;\">Att Counterparty</td><td></td>" +
                    "</tr><tr>" +
                        "<td style=\"font-weight:bold;\">Exposure</td><td style=\"text-align:center\">" + state0.currency + " " + state0.exposure + "</td><td style=\"font-weight:bold;\"></td><td style=\"text-align:center\"></td><td style=\"font-weight:bold;\"></td><td></td>" +
                    "</tr><tr>" +
                        "<td style=\"font-weight:bold;\">Call Margin</td><td style=\"text-align:center\">" + state0.currency + " " + state0.marginCall + "</td><td style=\"font-weight:bold;\"></td><td style=\"text-align:center\"></td><td style=\"font-weight:bold;\"></td><td></td>" +
                     "</tr>" +
                "</table>" +
                "</div></br></br>"

        html = html + "<table id=\"tbHistorico\" class=\"table table-bordered table-striped\">" +
                "<thead>" +
                    "<tr>" +
                        "<th style=\"text-align:center; display:none\">ID_P</th>" +
                        "<th style=\"text-align:center; display:none\">ID_C</th>" +
                        "<th style=\"text-align:center\">Actioner</th>" +
                        "<th style=\"text-align:center\">Exposure</th>" +
                        "<th style=\"text-align:center\">Margin Call</th>" +
                        "<th style=\"text-align:center\">Issued Date</th>" +
                        "<th style=\"text-align:center;\">Attachment</th>" +
                        "<th style=\"text-align:center; display:none\">Attachment Counterparty</th>" +
                        "<th style=\"text-align:center\">Status</th>" +
                        "<th style=\"text-align:center\">Event Date</th>" +
                        "<th style=\"text-align:center\">Amount Paid</th>" +
                    "</tr>" +
                "</thead>" +
                "<tbody>";

        $.ajax({
            type: "GET",
            dataType: "json",
            url: url + node + "api/collateral/historico?contractAddress=" + contractAddress,
            success: function (response) {
                for (i = 0; i < response.length; i++) {
                    var state = response[i];
                   
                    html = html + " <tr id=" + state.contractAddress + ">" +
                                                "<td style=\"text-align:center;display:none\">" + state.idExternalParty + "</td>" +
                                                "<td style=\"text-align:center;display:none\">" + state.idExternalCTPY + "</td>" +
                                                "<td style=\"text-align:center\">" + (state.status == "Margin Call" ? state.party : (response[i - 1].status == "Margin Call") ? state.ctpy : state.party) + "</td>" +
                                                "<td style=\"text-align:center\">" + state.currency + " " + state.exposure + "</td>" +
                                                "<td style=\"text-align:center\">" + state.currency + " " + state.marginCall + "</td>" +
                                                "<td style=\"text-align:center\">" + state.callDate + "</td>" +
                                                "<td style=\"text-align:center;\"><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"getDocument('" + p_Party + "','" + "" + "','" + "" + "')\">" + "" + "</button></td>" +
                                                "<td style=\"text-align:center;display:none\">" + state.idExternalCTPY + "</td>" +
                                                "<td style=\"text-align:center\">" + state.status + "</td>" +
                                                "<td style=\"text-align:center\">" + state.timeStamp + "</td>" +
                                                "<td style=\"text-align:center\">" + state.amountSettle + "</td>" +
                                            "</tr>";
                }

                html = html + "</tbody>" +
                                 "</table>";

                showBSModal({
                    title: "State Chain <p>Margin Call ID    [" + contractAddress + "]</p> <p>Status    [<b>" + state0.status + "</b>]</p> <p>Comments    [<b>" + state0.comments + "</b>]</p>",
                    body: html,
                    size: "large"
                });
            }
        });
    });
}

function getDocument(p_Party, p_SecureHash, p_Name) {
    var node = "";

    if (p_Party == "Itau") {
        node = "10007"
    } else {
        node = "10010"
    }

    var api = url + ":" + node + "/api/collateral/get-document?fileName=" + p_Name + "&fileHash=" + p_SecureHash;

    window.location.href = api
}